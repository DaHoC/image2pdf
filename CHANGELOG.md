# Changelog

Changes:

* v1.2:
  - respect EXIF orientation
  - fix conversion in case of missing pdf extension
* v1.1:
  - added error messages thanks to robertoldham
  - added troubleshooting for missing ImageMagick permissions
  - added source code to VCS
* v1.0:
  - updated instructions on how to install in KDE5/Plasma
  - spanish translation clarified thanks to jorgebarroso
* v0.9:
  - russian language support added thanks to JustFoxy
* v0.8:
  - fixed counting of files
* v0.7:
  - disabled erroneous message on user abort
* v0.6:
  - italian language support added thanks to kdekda
* v0.5:
  - french language support added thanks to garions
* v0.4:
  - spanish language support added thanks salvamendo
* v0.3:
  - mandarin language support added thanks to littlechiny
* v0.2:
  - initial commit 
