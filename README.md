# image2pdf

KDE (Plasma) Dolphin addon, that converts images to pdfs with one click (using imagemagick http://www.imagemagick.org).

You can select many images that either will be converted to one single pdf file or alternatively each image results in a separate pdf

For more info, see https://store.kde.org/p/998424

## Installation instructions

### Preconditions

1. You have to have the imagemagick (http://www.imagemagick.org) package installed because this script depends on the convert command.
2. ImageMagic-6 on Ubuntu 18.04 and more recent does not grant permission to edit PDF files through convert by default anymore:
You have to configure permissions for ImageMagic in `/etc/ImageMagic-6/policy.xml`:
Change the line

```xml
<policy domain="coder" rights="none" pattern="PDF" />
```

to

```xml
<policy domain="coder" rights="write" pattern="PDF" />
```


### Installation for KDE5/Plasma:

#### User installation:
- place the .desktop file in `~/.local/share/kservices5/ServiceMenus/`:

```shell
cp image2pdf.desktop ~/.local/share/kservices5/ServiceMenus/
```

#### System-wide installation:
- place the .desktop file in `/usr/share/kservices5/ServiceMenus/`:

```shell
sudo cp image2pdf.desktop /usr/share/kservices5/ServiceMenus/
```

If you have trouble getting ServiceMenus to work in Dolphin after upgrade to KDE5/Plasma, following option worked for me:

```shell
sudo cp /usr/share/kde4/servicetypes/konqpopupmenuplugin.desktop /usr/share/kservicetypes5/
```

For KDE5, running `kbuildsycoca5` should then make the ServiceMenu immediately available.


### Installation for KDE4:

#### User installation:
- place the .desktop file in `$(kde4-config --localprefix)/share/kde4/services/ServiceMenus/`

#### System-wide installation:
- place the .desktop file in `$(kde4-config --prefix)/share/kde4/services/ServiceMenus/`

It may take some moments for KDE4 to update the service menus after the file has been copied.


## Troubleshooting

If you encounter error

> The conversion failed with error convert-im6.q16: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/408.

You have to grant permissions to write to PDF files for ImageMagic in `/etc/ImageMagic-6/policy.xml`:

Change the line

```xml
<policy domain="coder" rights="none" pattern="PDF" />
```

to

```xml
<policy domain="coder" rights="write" pattern="PDF" />
```
