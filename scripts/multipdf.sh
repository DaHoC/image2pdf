#!/usr/bin/env sh
count=0;
errorCount=0;
errorMessage="";
for f in %F; do
  result=$(convert "$f" -auto-orient "${f%.*}.pdf" 2>&1);
  if [ $? -eq 0 ]; then
    count=$((count+1));
  else
    errorCount=$((errorCount+1));
    errorMessage=${result};
  fi;
done;
if [ $errorCount -eq 0 ]; then
  kdialog --title "image2pdf" --passivepopup "Converted $count image(s) into PDF files!";
else
  kdialog --title "image2pdf error" --msgbox "The conversion failed with $errorCount error(s): $errorMessage";
fi;
