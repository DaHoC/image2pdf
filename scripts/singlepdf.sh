#!/usr/bin/env sh
count=0;
for f in %F; do
  fdir=$(dirname "$f");
  count=$((count+1));
done;
cd "$fdir" || exit;
targetFileName=$(kdialog --getsavefilename "$fdir" "application/pdf");
if [ $? -eq 0 ]; then
  targetFileName="${targetFileName%.*}.pdf";
  result=$(convert %F -auto-orient "$targetFileName" 2>&1);
  if [ $? -eq 0 ]; then
    kdialog --title "image2pdf" --passivepopup "Converted $count image(s) into '$targetFileName'!";
  else
    kdialog --title "image2pdf error" --msgbox "The conversion failed with error: $result";
  fi;
else
  echo "Aborted";
fi;
